module Data.Hashes.FNV1a where

import Data.ByteString          (ByteString)
import Data.Bits                (shiftR, xor, (.&.))
import Data.Word                (Word64, Word32, Word16, Word8)

import qualified Data.ByteString as B

fnvPrime32        :: Word32
fnvPrime32        = 16777619

fnvOffsetBasis32  :: Word32
fnvOffsetBasis32  = 2166136261

fnvPrime64        :: Word64
fnvPrime64        = 1099511628211

fnvOffsetBasis64  :: Word64
fnvOffsetBasis64  = 14695981039346656037

fnvPrime128       :: Integer
fnvPrime128       = 309485009821345068724781371

fnvOffsetBasis128 :: Integer
fnvOffsetBasis128 = 144066263297769815596495629667062367629

fnv1aHash32Word32 :: Word32 -> Word32 -> Word32
fnv1aHash32Word32 base word =
    let b0 = fromIntegral $ word               .&. 0xFF
        b1 = fromIntegral $ (word `shiftR` 8)  .&. 0xFF
        b2 = fromIntegral $ (word `shiftR` 16) .&. 0xFF
        b3 = fromIntegral $ (word `shiftR` 24) .&. 0xFF
    in foldl fnv1aHash32Base base [b0, b1, b2, b3]

fnv1aHash32Word16 :: Word32 -> Word16 -> Word32
fnv1aHash32Word16 base hword =
    let b0 = fromIntegral $ hword .&. 0xFF
        b1 = fromIntegral $ (hword `shiftR` 8) .&. 0xFF
    in foldl fnv1aHash32Base base [b0, b1]

fnv1aHash32Base :: Word32 -> Word8 -> Word32
fnv1aHash32Base base byte = (base `xor` (fromIntegral byte)) * fnvPrime32

fnv1aHash32 :: ByteString -> Word32
fnv1aHash32 = B.foldl hash fnvOffsetBasis32
    where hash base = (fnv1aHash32Base base)

fnv1aHash64 :: ByteString -> Word64
fnv1aHash64 = B.foldl hash fnvOffsetBasis64
    where hash base val = (base `xor` (fromIntegral val)) * fnvPrime64

fnv1aHash128 :: ByteString -> Integer
fnv1aHash128 = B.foldl hash fnvOffsetBasis128
    where mask          = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          hash base val = (base `xor` (fromIntegral val)) * fnvPrime128 .&. mask
