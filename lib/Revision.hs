module Revision (getRevTH)
where

import Control.Exception (try)
import Data.Char (isSpace)
import Data.List (dropWhileEnd)
import Language.Haskell.TH (ExpQ, runIO, stringE)
import System.Process (readProcessWithExitCode)

getRev :: IO String
getRev = recover <$> (try $ readProcessWithExitCode "get-rev" [] "")
  where
    recover (Left (_ :: IOError)) = "UNKNOWN"
    recover (Right (_, stdout, _)) = dropWhileEnd isSpace stdout

getRevTH :: ExpQ
getRevTH = stringE =<< runIO getRev

